#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


def to_5adic(n, digit):
    ans = [] # type: List[int]
    while n > 0:
        ans.append(n % 5)
        n = n // 5
    if len(ans) < digit:
        ans += [0] * (digit-len(ans))
    return ans

def modifier(digit):
    s = 0
    for i in range(digit):
        s += 8*(5**i)
    return s

def decide(S, digit):
    T = S - modifier(digit)
    debug(T)
    list_5adic = to_5adic(T, digit)
    return list(map(lambda n: n%2,
                    list_5adic))

# S = 9 + 10*5 + 11*(5**2)
# debug(decide(S, 3))

def get_query(M, start, length):
    query_list = ["?"]
    for i in range(M):
        if i < start:
            q = "0"
        elif start <= i < start+length:
            q = str(5**(i-start))
        else:
            q = "0"
        query_list.append(q)
    return " ".join(query_list)


d = 5

N = int(input())
len_list = ([d]*(N//d))
if N%d != 0:
    len_list += [N%d]
start = 0
ans = [] # type: List[int]
for l in len_list:
    query = get_query(N, start, l)
    print(query)
    S = int(input())
    ans += decide(S, l)
    start += l

hoge = ["!"]  +list(map(lambda n: str(n),
                        ans))
print(" ".join(hoge))
