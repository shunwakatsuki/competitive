#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


n, m = map(int, input().split())
a = list(map(int, input().split()))
b = list(map(int, input().split()))


a_s = [0]
for i in range(1, n):
    if a[i-1] == a[i]:
        continue
    else:
        a_s.append(i)
a_s.append(n)
b_s = [0]
for i in range(1, m):
    if b[i-1] == b[i]:
        continue
    else:
        b_s.append(i)
b_s.append(m)

a_ws = []
for i in range(len(b_s)-1):
    a_ws.append(b_s[i+1] - b_s[i])
b_ws = []
for i in range(len(a_s)-1):
    b_ws.append(a_s[i+1] - a_s[i])


a_sum = sum(b)
b_sum = sum(a)


memo = {}  # type: Dict[Tuple[int, int, int], int]
def part(s, n, A):
    if n == 0:
        if s == 0:
            return 1
        return 0
    if (s, n, A) in memo.keys():
        return memo[(s, n, A)]
    ans = 0
    for k in range(0, A+1):
        ans += part(s-k, n-1, k)
    memo[(s, n, A)] = ans
    return ans


dp = [{0:1}]
for i in range(len(a_ws)):
    for N in range(a_sum+1):
        pass
