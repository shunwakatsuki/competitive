#!/usr/bin/env python3
import copy

A, B = map(int, input().split())

gcd_memo = {}
def gcd(a, b):
    if a > b:
        a, b = b, a
    if (a, b) in gcd_memo:
        return gcd_memo[(a, b)]
    g = _gcd(a, b)
    gcd_memo[(a, b)] = g
    return g

def _gcd(a, b):
    # assert(a <= b)
    if b%a == 0:
        return a
    else:
        return _gcd(b%a, a)

def coprime(l, i):
    for k in l:
        if gcd(k, i) > 1:
            return False
    return True

# dp = []
# dp.append([[], [A]])

# for i in range(1, B-A+1):
#     dp.append(copy.deepcopy(dp[i-1]))
#     for prev in dp[i-1]:
#         if coprime(prev, A+i):
#             dp[i].append(prev + [A+i])

dp = [[[], [A]], []]

for i in range(1, B-A+1):
    dp[i%2] = []# copy.deepcopy(dp[(i-1)%2])
    for prev in dp[(i-1)%2]:
        if coprime(prev, A+i):
            dp[i%2].append(prev + [A+i])
    dp[i%2] = dp[i%2] + dp[(i-1)%2]


# print(dp)

print(len(dp[(B-A)%2]))
