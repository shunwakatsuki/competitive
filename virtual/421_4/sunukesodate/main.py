#!/usr/bin/env python3
import copy
from itertools import chain

A, B = map(int, input().split())

gcd_memo = {}
def gcd(a, b):
    if a > b:
        a, b = b, a
    if (a, b) in gcd_memo:
        return gcd_memo[(a, b)]
    g = _gcd(a, b)
    gcd_memo[(a, b)] = g
    return g

def _gcd(a, b):
    # assert(a <= b)
    if b%a == 0:
        return a
    else:
        return _gcd(b%a, a)

def coprime(l, i):
    for k in l:
        if gcd(k, i) > 1:
            return False
    return True

def get_coprime_groups(l, i):
    print(l,i)
    if coprime(l,i):
        return [l+[i]]
    ll = map(lambda l_: get_coprime_groups(l_,i), [l[:j]+l[j+1:] for j in range(len(l)-1)])
    return list(chain.from_iterable(ll))

res = [[]]
for k in range(A, B+1):
    res = list(chain.from_iterable(map(lambda l_: get_coprime_groups(l_,k),
                                       res)))
print(res)
