#!/usr/bin/env python3
import sys

N = int(input())
A = list(map(int, input().split()))

num_even = 0
num_odd = 0

for i in range(N):
    if A[i] % 2 == 0:
        num_even += 1
    else:
        num_odd += 1

if (num_even == 0) and (num_odd == 0):
    print("NO")
    sys.exit(0)
if (num_even == 1) and (num_odd == 0):
    print("YES")
    sys.exit(0)
if num_odd == 1 and num_even == 0:
    print("YES")
    sys.exit(0)

if num_odd % 2 == 1:
    print("NO")
    sys.exit(0)

print("YES")
