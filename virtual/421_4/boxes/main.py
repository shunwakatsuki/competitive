#!/usr/bin/env python3
import sys

N = int(input())
A = list(map(int, input().split()))

S = N*(N+1) // 2
SA = sum(A)

if SA % S != 0:
    print("NO")
    sys.exit(0)

K = SA // S

# B = []
C = []
for i in range(N):
    # B.append(A[i] - A[i-1] - K)
    b = A[i] - A[i-1] - K
    if (b > 0) or (-b % N != 0):
        print("NO")
        sys.exit(0)
    C.append(-b//N)

print("YES")
