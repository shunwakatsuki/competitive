#!/usr/bin/env python3
import sys

N, M = map(int, input().split())
# a = []
# b = []
count = [True] * N
for m in range(M):
    a_, b_ = map(int, input().split())
    a_ -= 1
    b_ -= 1
    # a.append(a_)
    # b.append(b_)
    count[a_] = count[a_] ^ True
    count[b_] = count[b_] ^ True

# count = [0] * N
# for m in range(M):
#     count[a[m]]
for m in range(M):
    if not count[m]:
        print("NO")
        sys.exit(0)
print("YES")
