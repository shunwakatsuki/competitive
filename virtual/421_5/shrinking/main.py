#!/usr/bin/env python3

s = input()


def min_shrink(a):
    ind = [-1] + [i for i, x in enumerate(s) if x == a] + [len(s)]
    x = []
    for i in range(len(ind)-1):
        x.append(ind[i+1]-ind[i]-1)
    # print(a, max(x))
    return max(x)

ans = 1000
for a in "abcdefghijklmnopqrstuvwxyz":
    ans = min(ans, min_shrink(a))

print(ans)
