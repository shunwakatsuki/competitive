#!/usr/bin/env python3

N, M = map(int, input().split())
a = [0]*M
b = [0]*M
c = [0]*M
for m in range(M):
    a[m], b[m], c[m] = map(int, input().split())
    if a[m] > b[m]:
        a[m], b[m] = b[m], a[m]
        assert(a[m] < b[m])
    a[m] -= 1
    b[m] -= 1
#
temp = [10**7] * N
d = [temp[:] for i in range(N)]

for i in range(N):
    for j in range(i, N):
        assert(i<=j)
        if i == j:
            d[i][j] = 0
        else:
            for m in range(M):
                if a[m]==i and b[m]==j:
                    d[i][j] = c[m]

for k in range(N):
    for i in range(N):
        for j in range(i, N):
            assert(i<=j)
            if i <= k:
                dik = d[i][k]
            else:
                dik = d[k][i]
            if k <= j:
                dkj = d[k][j]
            else:
                dkj = d[j][k]
            if d[i][j] > dik + dkj:
                d[i][j] = dik + dkj

ans = 0
for m in range(M):
    if c[m] > d[a[m]][b[m]]:
        ans += 1
    elif c[m] < d[a[m]][b[m]]:
        raise Exception()
print(ans)
