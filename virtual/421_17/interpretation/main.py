#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


class UnionFind:
    def __init__(self, N):
        # type: (int) -> None
        self.N = N
        self.parent = list(range(N))  # type: List[int]

    def root(self, x):
        # type: (int) -> int
        path_to_root = []
        while self.parent[x] != x:
            path_to_root.append(x)
            x = self.parent[x]
        for node in path_to_root:
            self.parent[node] = x  # パス圧縮
        return x

    def same(self, x, y):
        # type: (int, int) -> bool
        return self.root(x) == self.root(y)

    def unite(self, x, y):
        # type: (int, int) -> None
        self.parent[self.root(x)] = self.root(y)

    def __str__(self):
        # type: () -> str
        groups = {}  # type: Dict[int, List[int]]
        for x in range(self.N):
            root = self.root(x)
            if root in groups.keys():
                groups[root].append(x)
            else:
                groups[root] = [x]
        result = ""
        for root in groups.keys():
            result += str(groups[root]) + "\n"
        return result

    def __len__(self):
        # type: () -> int
        roots = list(map(lambda x: self.root(x),
                         range(self.N)))
        return len(set(roots))


N, M = map(int, input().split())
K = []
L = []
for i in range(N):
    temp = list(map(int, input().split()))
    K.append(temp[0])
    L.append(list(map(lambda x: x-1,
                      temp[1:])))


speaker = [[] for i in range(M)]  # type: List[List[int]]


for i in range(N):
    for k in range(K[i]):
        speaker[L[i][k]].append(i)

u = UnionFind(N)
for l in range(M):
    sp = speaker[l]
    if len(sp) > 0:
        for i in range(1, len(sp)):
            u.unite(sp[0], sp[i])
if len(u) == 1:
    print("YES")
else:
    print("NO")
