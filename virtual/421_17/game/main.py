#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


s = input()


if s[0] == s[-1]:
    if len(s) % 2 == 0:
        print("First")
    else:
        print("Second")
else:
    if len(s) % 2 == 0:
        print("Second")
    else:
        print("First")
