#!/usr/bin/env python3
import sys
from time import sleep

H, M, S = map(int, input().split())
C1, C2 = map(int, input().split())

# if C2 == 0:
#     sleep(0.5)

def isint(x):
    return float(x).is_integer()

def myfloor(x):
    # if isint(x):
    #     # sleep(0.3)
    #     return x-1
    # return int(x)
    return int(x-0.0001)
def myceil(x):
    # if isint(x):
    #     # sleep(0.5)
    #     return x+1
    # return int(x)+1
    return int(x+0.0001)+1

theta_s = 1/60
theta_m = 1/3600
theta_h = 1/(12*3600)

def csm(s):
    return int(s*(theta_s - theta_m))
def cmh(s):
    return int(s*(theta_m - theta_h))

s = S + M*60 + H*3600

min1 = myceil((C1 + csm(s)) / (theta_s - theta_m))
max1 = myfloor((C1 + csm(s)+1) / (theta_s - theta_m))

min2 = myceil((C2 + cmh(s)) / (theta_m - theta_h))
max2 = myfloor((C2 + cmh(s)+1) / (theta_m - theta_h))
if min2 < s:
    min2 = s

if (max1 < min2) or (max2 < min1):
    print(-1)
    sys.exit(0)

min_ = max(min1, min2)
max_ = min(max1, max2)

print(min_-s, max_-s)
