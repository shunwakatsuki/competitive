#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N, Z, W = map(int, input().split())
a = list(map(int, input().split()))

if N == 1:
    print(abs(a[-1]-W))
    exit()

print(max(abs(a[-2] - a[-1]),
          abs(a[-1] - W)))
