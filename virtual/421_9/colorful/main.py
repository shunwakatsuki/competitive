#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N = int(input())
a = list(map(int, input().split()))

min_ = min(a)
max_ = max(a)

if max_ > min_ + 1:
    print("No")
    exit()

if min_ == max_:
    if min_ == N-1:
        print("Yes")
        exit()
    if 2*min_ <= N:
        print("Yes")
        exit()
    print("No")
    exit()
else:
    # max_ == min_ + 1
    count_min = a.count(min_)
    count_max = a.count(max_)
    if count_min >= max_:
        print("No")
        exit()
    if count_min + 2*(max_ - count_min) <= N:
        debug("hoge")
        print("Yes")
        exit()
    print("No")
