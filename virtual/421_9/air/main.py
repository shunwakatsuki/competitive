#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N, C, K = map(int, input().split())
T = []
for i in range(N):
    T.append(int(input()))

T.sort()

start = 0
end = 0
count = 1
while True:
    if end >= N-1:
        break
    if (end - start + 1 >= C) or (T[end+1] > T[start] + K):
        debug(start, end)
        start = end + 1
        end = start
        if start == N:
            break
        else:
            count += 1
    else:
        end += 1

print(count)
