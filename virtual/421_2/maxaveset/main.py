#!/usr/bin/python3

from math import factorial as f

N, A, B = map(int, input().split())
v = list(map(int, input().split()))

v.sort()


sum_v = 0
for i in range(N-A+1, N):
    sum_v += v[i]

i_max = N+1
count_max = 1
sum_max = 0

max_nums = []
if A == 1:
    current_num = -1
    current_count = 0
else:
    current_num = v[N-A+1]
    current_count = v[N-A+1:].count(v[N-A+1])
for i in range(N-A, N-B-1, -1):
    if v[i] == current_num:
        current_count += 1
    else:
        current_num = v[i]
        current_count = 1
    count = N-i
    sum_v += v[i]
    if sum_v * count_max > sum_max * count:
        count_max = count
        sum_max = sum_v
        max_nums = [(v[i], current_count)]
    elif sum_v * count_max == sum_max * count:
        if v[i] not in max_nums:
            max_nums.append((v[i], current_count))

def count(vi):
    return v.count(vi)

def c(n, k):
    return f(n)//(f(k)*f(n-k))

ans = 0
for vi, cc in max_nums:
    ans = ans + c(count(vi), cc)
    # print(vi, count(vi), cc)

print(sum_max/count_max)
print(ans)
