#!/usr/bin/python3

from math import factorial as f

N, P = list(map(int, input().split()))
A = list(map(int, input().split()))

num_odd = 0
num_even = 0
for i in range(N):
    if A[i] % 2 == 0:
        num_even += 1
    else:
        num_odd += 1

even_choice = 2**num_even

def c(n, k):
    return f(n)//(f(k)*f(n-k))

odd_choice = 0
for i in range(0, num_odd+1):
    if i % 2 == P:
        odd_choice += c(num_odd, i)

print(even_choice*odd_choice)
