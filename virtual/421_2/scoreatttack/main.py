#!/usr/bin/env python3
# from math import inf
inf = 10**50

N, M = map(int, input().split())
edges = []
for j in range(M):
    edges.append(list(map(int, input().split())))

d = [0] + ([-inf]*(N-1))
pred = [None] * N

for i in range(N-1):
    for j in range(M):
        u = edges[j][0]-1
        v = edges[j][1]-1
        w = edges[j][2]
        if (d[v] < d[u] + w) and (d[u] != -inf):
            d[v] = d[u] + w
            pred[v] = u

positive_loop = False
for i in range(N):
    for j in range(M):
        u = edges[j][0]-1
        v = edges[j][1]-1
        w = edges[j][2]
        if (d[u] + w > d[v]) and (d[u] != -inf):
            d[v] = d[u] + w
            if v == N-1:
                positive_loop = True
                break
    if positive_loop:
        break

if positive_loop:
    print("inf")
else:
    print(d[N-1])
