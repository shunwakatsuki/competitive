#!/usr/bin/env python3
import sys

N = int(input())
A = list(map(int, input().split()))

if N <= 2:
    print(1)
    sys.exit(0)

count = 0
status = 0
for i in range(1,N):
    # print(A[i-1],A[i], status)
    if A[i-1] == A[i]:
        new_status = 0
        continue
    elif A[i-1] < A[i]:
        new_status = 1
    elif A[i-1] > A[i]:
        new_status = -1
    if status == 0:
        status = new_status
        continue
    if status == new_status:
        continue
    else:
        count += 1
        # print("c")
        status = 0

print(count+1)
