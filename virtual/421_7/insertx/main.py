#!/usr/bin/env python3
import sys

s = input()

swox = "".join(list(filter("x".__ne__, list(s))))

# print(s, swox)
def is_palind(a):
    for i in range(len(a)//2):
        if a[i] != a[-i-1]:
            return False
    return True

if not is_palind(swox):
    print(-1)
    sys.exit(0)
#
l = 0
r = -1
count = 0
# print(s)
while True:
    # print(l, r+len(s))
    # print(s[l],s[r])
    if l >= r+len(s):
        break
    if s[l] == "x" and s[r] == "x":
        l += 1
        r -= 1
    elif s[l] == "x":
        l += 1
        count += 1
    elif s[r] == "x":
        r -= 1
        count += 1
    else:
        l += 1
        r -= 1
print(count)
