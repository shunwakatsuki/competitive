#!/usr/bin/env python3
import sys

H, W = map(int, input().split())
a = ""
for i in range(H):
    a += input()

# count = {}
count_all = len(a)
count_4 = 0
count_2 = 0
count_1 = 0

for s in "abcdefghijklmnopqrstuvwxyz":
    # count[s] = a.count(s)
    c = a.count(s)
    count_4 += c//4
    c = c%4
    count_2 += c//2
    c = c%2
    count_1 += c

if W%2 == 1 and H%2 == 0:
    W, H = H, W

# sys.stderr.write(str(321))
if W%2 == 0 and H%2 == 0:
    sys.stderr.write(str((count_4, W*(H-1)//4)))
    if count_4 < W*H//4:
        print("No")
    else:
        print("Yes")
elif W%2 == 0 and H%2 == 1:
    if count_4 < W*(H-1)//4:
        # print(count_4, W*(H-1)//4)
        sys.stderr.write(str((count_4, W*(H-1)//4)))
        print("No")
    elif 2*(count_4 - W*(H-1)//4) + count_2 < W//2:
        print("No")
    else:
        print("Yes")
else:
    if count_4 < (W-1)*(H-1)//4:
        print("No")
    elif 2*(count_4 - (W-1)*(H-1)//4) + count_2 < (W+H-2)//2:
        print("No")
    else:
        print("Yes")
