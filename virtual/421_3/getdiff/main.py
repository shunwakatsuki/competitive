#!/usr/bin/env python3

N, K = map(int, input().split())
A = list(map(int, input().split()))

def gcd(a, b):
    if a > b:
        a, b = b, a
    # assert(a <= b)
    if b%a == 0:
        return a
    else:
        return gcd(b%a, a)

def gcd_many(l):
    # if len(l) == 1:
    #     return l
    # elif len(l) == 2:
    #     return gcd(l[0], l[1])
    # else:
    #     return gcd(l[0], gcd_many(l[1:]))
    if len(l) == 1:
        return l[0]
    g = l[0]
    for i in range(1,len(l)):
        g = gcd(g, l[i])
    return g

Amax = max(A)
g = gcd_many(A)

possible = "POSSIBLE"
impossible = "IMPOSSIBLE"


if K > Amax:
    print(impossible)
else:
    if K % g == 0:
        print(possible)
    else:
        print(impossible)
