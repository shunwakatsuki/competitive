#!/usr/bin/env python3

A = str(input())
n = len(A)

num_sym = 0

def sym(i, j):
    for k in range(i, i+(j-i)//2+3):
        if (i <= k <= j) and (A[k] != A[j-k+i]):
            return False
    return True

for i in range(n):
    for j in range(i+1, n):
        if sym(i,j):
            num_sym += 1

c = n*(n-1)//2
print(1+c-num_sym)
