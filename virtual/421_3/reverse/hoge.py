s = "abracadabra"

l = [(s, None)]

def rev(t, i, j):
    return t[:i] + "".join(list(reversed(t[i:j+1]))) + t[j+1:]

for i in range(len(s)-1):
    for j in range(i, len(s)):
        l.append((rev(s, i, j), i, j))

print(sorted(l, key=lambda x: x[0]))
print(sorted(list(set(l)), key=lambda x: x[0]))
