#!/usr/bin/env python3
import sys

N, A, B = map(int, input().split())

if A > B:
    print(0)
    sys.exit(0)

if N == 1:
    if A == B:
        print(1)
        sys.exit(0)
    else:
        print(0)
        sys.exit(0)

max_ = A + (N-1)*B
min_ = A*(N-1) + B

print(max_ - min_ + 1)
