#!/usr/bin/env python3
import copy

N, Ma, Mb = map(int, input().split())
a = []
b = []
c = []
for i in range(N):
    ai, bi, ci = map(int, input().split())
    a.append(ai)
    b.append(bi)
    c.append(ci)

def update(mincosti, a_b, c):
    if a_b not in mincosti:
        mincosti[a_b] = c
        return
    if mincosti[a_b] > c:
        mincosti[a_b] = c
        return

mincost = [{(a[0],b[0]): c[0]}]

for i in range(1,N):
    mincost.append(copy.deepcopy(mincost[i-1]))
    update(mincost[i], (a[i],b[i]), c[i])
    for a_, b_ in mincost[i-1].keys():
        a_b = (a_+a[i], b_+b[i])
        update(mincost[i], a_b, mincost[i-1][(a_,b_)]+c[i])

Na = Ma
Nb = Mb

ans = None

while (Na <= 400) and (Nb <= 400):
    if (Na, Nb) in mincost[N-1].keys():
        if ans == None:
            ans = mincost[N-1][(Na, Nb)]
        else:
            ans = min(ans, mincost[N-1][(Na, Nb)])
    Na += Ma
    Nb += Mb

if ans == None:
    print(-1)
else:
    print(ans)
