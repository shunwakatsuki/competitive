#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N = int(input())
a = list(map(int, input().split()))

a.sort()

print(sum([a[3*N-2*i-2] for i in range(N)]))
