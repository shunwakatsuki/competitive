#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N, M = map(int, input().split())
A = []
for i in range(N):
    A.append(list(map(int, input().split())))

def is_possible(attend):
    jisshi = [None] * M
    for i in range(N):
        s = attend[i]
        if jisshi[s] is False:
            return False
        jisshi[s] = True
        j = A[i].index(s)
        for k in range(j):
            if jisshi[A[i][k]] is True:
                return False
            jisshi[A[i][k]] = False
    return True

attend_dp = {}
def get_attend(i, n):
    if (i, n) in attend_dp.keys():
        return attend_dp[(i,n)]
    if i == 1:
        attend_dp[(1,n)] = [[j] for j in range(M)]
        return attend_dp[(1,n)]
    res = []
    for a in get_attend(i-1, n-1):
        for j in range(M):
            b = a + [j]
            if is_possible(b) and max_count(b) == n:
                res.append(b)
    for a in get_attend(i-1, n) and max_count(b) == n:
        for j in range(M):
            b = a + [j]
            if is_possible(b):
                res.append(b)
    attend_dp[(i,n)] = res
    return attend_dp[(i,n)]

def max_count(b):
    c = sorted(b)
    return c.count(c[-1])

for n in range(1, N+1):
    if get_attend(N, n):
        print(n)
        exit()
