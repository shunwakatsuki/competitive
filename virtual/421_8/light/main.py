#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

sys.setrecursionlimit(100000)

N, X = map(int, input().split())

def f(x, n):
    if x == 0 or x == n:
        return 0
    debug(x,n)
    # if 2*x == n:
    #     return 3*x
    if x % (n-x) == 0:
        return 3*x
    if 2*x < n:
        # return f(x, n-x) + 3*x# n + x
        return f(n-x, n)
    a = x//(n-x) + 1
    t = a*(n-x) - x
    return f(n-x-t, n-x) + 3*x # + t

print(f(X, N))
