#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N = int(input())
A = []
count = [None, 0, 0, 0, 0]
for i in range(N):
    A.append(int(input()))
    count[A[i]] += 1
A.reverse()

ans = 0
while A:
    ans += 1
    a = A.pop()
    count[a] -= 1
    if a == 4:
        continue
    elif a == 3:
        if count[1] >= 1:
            A.remove(1)
            count[1] -= 1
    elif a == 2:
        if count[2] >= 1:
            A.remove(2)
            count[2] -= 1
        elif count[1] >= 2:
            A.remove(1)
            A.remove(1)
            count[1] -= 2
        elif count[1] >= 1:
            A.remove(1)
            count[z] -= 1
    elif a == 1:
        if count[3] >= 1:
            A.remove(3)
            count[3] -= 1
            continue
        if count[2] >= 1:
            A.remove(2)
            count[2] -= 1
            a += 2
        while (a < 4) and (count[1] >= 1):
            A.remove(1)
            count[1] -= 1
            a += 1
        # elif (count[2] >= 1) and (count[1] >= 1):
        #     A.remove(2)
        #     A.remove(1)
        #     count[2] -= 1
        #     count[1] -= 1

print(ans)
