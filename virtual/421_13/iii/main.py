#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


n, m = map(int, input().split())
x = list(map(int, input().split()))
y = list(map(int, input().split()))
p = 10**9 + 7


xsum = 0
for i in range(n-1):
    xsum = (xsum + (x[i+1] - x[i]) * (i+1) * (n-i-1)) % p

ysum = 0
for j in range(m-1):
    ysum = (ysum + (y[j+1] - y[j]) * (j+1) * (m-j-1)) % p


ans = (xsum * ysum) % p
print(ans)
