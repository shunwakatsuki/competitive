#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


H, W, h, w = map(int, input().split())
yes, no = "Yes", "No"

if (H%h == 0) and (W%w == 0):
    print(no)
    exit()

print(yes)

unit = 1000
if (W%w != 0):
    for i in range(H):
        # l_ = [str(unit)] * (w-1)
        # l_ += [str(-unit*(w-1)-1)]
        # l = l_ * (W//w)
        # l += [str(unit)] * (W%w)
        l = []
        for j in range(W):
            if j % w == w-1:
                l.append(str(-unit*(w-1)-1))
            else:
                l.append(str(unit))
        print(" ".join(l))
    exit()
if (H%h != 0):
    for i in range(H):
        if i%h == h-1:
            l = [str(-unit*(h-1)-1)] * W
        else:
            l = [str(unit)] * W
        print(" ".join(l))
    exit()
raise Exception("hogeeee")
