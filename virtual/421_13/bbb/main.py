#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
A = []
for i in range(N):
    A.append(int(input()))


B = sorted(A)
d = {}


for i in range(N):
    d[A[i]] = i % 2

count = 0
for i in range(N):
    # d[B[i]] = (d[B[i]] + i) % 2
    count += (d[B[i]] + i) % 2


print(count//2)
