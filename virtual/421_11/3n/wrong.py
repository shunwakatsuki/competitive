#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


class Node:
    def __init__(self, val, l, r):
        self.val = val
        self.l = l
        self.r = r

    def min(self):
        if self.l is None:
            return self.val
        else:
            return self.l.min()

    def max(self):
        if self.r is None:
            return self.val
        else:
            return self.r.max()

    def popmin(self):
        if self.l is None:
            return self.r
        return Node(self.val, self.l.popmin(), self.r)

    def popmax(self):
        if self.r is None:
            return self.l
        return Node(self.val, self.l, self.r.popmax())

    def insert(self, x):
        if x < self.val:
            if self.l is None:
                return Node(self.val, Node(x, None, None), self.r)
            else:
                return Node(self.val, self.l.insert(x), self.r)
        else:
            if self.r is None:
                return Node(self.val, self.l, Node(x, None, None))
            else:
                return Node(self.val, self.l, self.r.insert(x))

    def sum(self):
        if self.l is None and self.r is None:
            return self.val
        if self.l is None:
            return self.val + self.r.sum()
        if self.r is None:
            return self.val + self.l.sum()
        return self.val + self.l.sum() + self.r.sum()

def make(list_):
    if len(list_) == 0:
        raise Exception("none")
    elif len(list_) == 1:
        return Node(list_[0], None, None)
    else:
        return make(list_[:-1]).insert(list_[-1])

#
if __name__ == '__main__':
    N = int(input())
    a = list(map(int, input().split()))

    if N == 1:
        print(max(a[0]-a[1], a[0]-a[2], a[1]-a[2]))
        exit()

    left = make(a[:N])
    right = make(a[2*N:])

    i = N
    j = 2*N-1

    for k in range(N):
        leftmin = left.min()
        rightmax = right.max()
        if leftmin < a[i]:
            if a[j] < rightmax:
                if (a[i]-leftmin) < (rightmax-a[j]):
                    right = right.popmax()
                    right = right.insert(a[j])
                    j -= 1
                else:
                    left = left.popmin()
                    left = left.insert(a[i])
                    i += 1
            else:
                left = left.popmin()
                left = left.insert(a[i])
                i += 1
        else:
            if a[j] < rightmax:
                right = right.popmax()
                right = right.insert(a[j])
                j -= 1
            else:
                if (leftmin-a[i]) < (a[j]-rightmax):
                    left = left.popmin()
                    left = left.insert(a[i])
                    i += 1
                else:
                    right = right.popmax()
                    right = right.insert(a[j])
                    j -= 1

    print(left.sum()-right.sum())
