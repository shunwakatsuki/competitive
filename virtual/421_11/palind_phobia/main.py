#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)
s = input()

a = s.count("a")
b = s.count("b")
c = s.count("c")

max_ = max(a, b, c)
min_ = min(a, b, c)

if max_ - min_ >= 2:
    print("NO")
else:
    print("YES")
