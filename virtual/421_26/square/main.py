#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


K = int(input())


def calc(radius):
    n = radius // K
    c = radius//2
    count = 0
    for i in range(n):
        for j in range(n):
            x = i*K
            y = j*K
            ok = True
            for a,b in [(x,y), (x+K,y),(x,y+K),(x+K,y+K)]:
                dist2 = (a-c)**2 + (b-c)**2
                if dist2 > (radius//2)**2:
                    ok = False
                    break
            if ok:
                count += 1
            # debug(x,y)
    return count


print(calc(200), calc(300))
