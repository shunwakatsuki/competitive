#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
a = []
for _ in range(N):
    a.append(int(input()))


ans = 0
for i in range(N):
    if a[i] % 2 == 0:
        ans += a[i] // 2
    else:
        if (i == N-1) or (a[i+1] == 0):
            ans += a[i] // 2
        else:
            ans += (a[i] // 2) + 1
            a[i+1] -= 1

print(ans)
