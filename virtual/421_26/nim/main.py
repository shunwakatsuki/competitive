#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
a = []
for _ in range(N):
    a.append(int(input()))

s = 0
for i in range(N):
    s = s ^ a[i]

if s == 0:
    print(0)
    exit()

d = 0
while True:
    if s < 2**d:
        break
    d += 1
d += 2

while d >= 0:
    if s & (2**d) != 0:
        pass                    # search
    d -= 1
