from itertools import combinations

N, K = map(int, input().split())
points = []
for i in range(N):
    points.append(list(map(int, input().split())))

points.sort(key=lambda p:p[0])


min_area = 10**20
for k in range(K, N+1):
    for l in range(0, N-k+1):
        points_tmp = points[l:l+k]
        points_tmp.sort(key=lambda p:p[1])
        for m in range(0, k-K+1):
            points_selected = points_tmp[m: m+K]
            ymin = points_selected[0][1]
            ymax = points_selected[-1][1]
            points_selected.sort(key=lambda p:p[0])
            xmin = points_selected[0][0]
            xmax = points_selected[-1][0]
            area = (xmax-xmin) * (ymax-ymin)
            min_area = min(area, min_area)

print(min_area)
