from itertools import combinations

N, K = map(int, input().split())
points = []
for i in range(N):
    points.append(list(map(int, input().split())))

xl = list(map(lambda p: p[0],
              points))
yl = list(map(lambda p: p[1],
              points))

def contained(point, xmin, xmax, ymin, ymax):
    return (xmin <= point[0] <= xmax) and (ymin <= point[1] <= ymax)

def count_contained(xmin, xmax, ymin, ymax):
    count = 0
    for point in points:
        if contained(point, xmin, xmax, ymin, ymax):
            count += 1
    return count

min_area = 10**20
for xs in combinations(xl, 2):
    xmin = min(xs)
    xmax = max(xs)
    for ys in combinations(yl, 2):
        ymin = min(ys)
        ymax = max(ys)
        if count_contained(xmin, xmax, ymin, ymax) >= K:
            area = (xmax-xmin) * (ymax-ymin)
            min_area = min(area, min_area)
print(min_area)
