import itertools

N = int(input())
eigyo = []
for i in range(N):
    eigyo.append(list(map(int, input().split())))
uriage = []
for i in range(N):
    uriage.append(list(map(int, input().split())))
# print(eigyo)
# print(uriage)

def get_kaburi(decide, i):
    kaburi = 0
    for j in range(10):
        if (decide[j] == 1) and (eigyo[i][j] == 1):
            kaburi += 1
    return kaburi

infinity = 10**10

def get_score(decide):
    if sum(decide) == 0:
        return -infinity
    score = 0
    total_kaburi = 0
    for i in range(N):
        kaburi = get_kaburi(decide, i)
        score += uriage[i][kaburi]
    return score

scores = map(get_score, itertools.product([0,1], repeat=10))
print(max(scores))
