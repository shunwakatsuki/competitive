s = str(input())
t = str(input())

s_sorted = "".join(sorted(s))
t_sorted = "".join(reversed(sorted(t)))

if s_sorted < t_sorted:
    print("Yes")
else:
    print("No")
