#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


x, y = map(int, input().split())


ans = -1

if y > 0:
    if y < x:
        ans = 1 + (-y) - (-x) + 1
    elif 0 <= x < y:
        ans = y - x
    elif -y <= x < 0:
        ans = 1 + y - (-x)
    elif x < -y:
        ans = (-y) - x + 1
    else:
        raise Exception("hoge")
elif y == 0:
    if x > 0:
        ans = 1 + x
    else:
        ans = -x
elif y < 0:
    if x < y:
        ans = y - x
    elif y < x < 0:
        ans = 1 + (-y) - (-x) + 1
    elif 0 <= x <= -y:
        ans = (-y) - x + 1
    elif -y < x:
        ans = 1 + y - (-x)
    else:
        raise Exception("fuga")

print(ans)
