#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
s = input()
t = input()


a = 0
for i in range(1, N+1):
    if s[N-i:] == t[:i]:
        a = i

print(2*N-a)
