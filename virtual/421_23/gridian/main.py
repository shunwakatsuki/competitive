#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


W, H = map(int, input().split())
p = []
for i in range(W):
    p.append(int(input()))
q = []
for j in range(H):
    q.append(int(input()))


a = []
for i in range(W):
    a.append(("p", i, p[i]))
for j in range(H):
    a.append(("q", j, q[j]))


a.sort(key=lambda x: x[2])


p_used = 0
q_used = 0
count = 0
cost = 0
i = 0
while True:
    now = a[i]
    if now[0] == "p":
        now_count = H+1 - q_used
        if count + now_count > (W+1)*(H+1) - 1:
            now_count = (W+1)*(H+1) - 1 - count
        count += now_count
        cost += now[2] * now_count
        p_used += 1
    else:
        now_count = W+1 - p_used
        if count + now_count > (W+1)*(H+1) - 1:
            now_count = (W+1)*(H+1) - 1 - count
        count += now_count
        cost += now[2] * now_count
        q_used += 1
    if count == (W+1)*(H+1) - 1:
        break
    i += 1
debug(count)
print(cost)
