#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)
sys.setrecursionlimit(10**5)


S = input()
nums = "0123456789"


def skip_num(s, i):
    while (i < len(s)) and (s[i] in nums):
        if i == len(s):
            break
        i += 1
    return i


def parse(s, i):
    # debug(i)
    # res = ""
    if s[i] in nums:
        j = skip_num(s, i)
        return (s[i:j], j)
    if s[i] in "+*-/":
        j = i+2
        args = []
        while True:  # s[j] != ")":
            t, k = parse(s, j)
            args.append(t)
            if s[k] == ",":
                j = k + 1
            else:
                j = k + 1
                break
        # debug("---", j, s[j])
        return ("(" + s[i].join(args) + ")",
                j)


test = "+(1)"
# test = "343214"
test = "+(3,-(1,2))"
test = "1"
test = "+(+(1,2),3)"
test = "+(+(+(+(1,1),1),1),1)"
debug(parse(test, 0))
# S = "*(1,2,+(3,4),5,*(6,7))"
print(parse(S, 0)[0])
