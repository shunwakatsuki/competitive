import sys
sys.setrecursionlimit(10**8)
from mytimer import MyTimer


def hoge(n: int) -> str:
    # print(n)
    if n == 0:
        return "hoge"
    elif n < 0:
        raise Exception("hogeeeee")
    return "((" + hoge(n-1) + "))"


if __name__ == '__main__':
    N = 10**5
    K = 10**2
    timer = MyTimer("foo")
    for i in range(K):
        hoge(N)
    # print(hoge(30))
    timer.end()
    MyTimer.show_all_timers()
