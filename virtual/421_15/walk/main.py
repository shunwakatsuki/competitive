#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N, A, B = map(int, input().split())
X = list(map(int, input().split()))


hirou = 0
for i in range(N-1):
    dist = X[i+1] - X[i]
    hirou += min(dist*A, B)


print(hirou)
