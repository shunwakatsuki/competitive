#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


X, Y = map(int, input().split())
X, Y = max(X, Y), min(X, Y)

Alice = "Alice"
Brown = "Brown"

if X-Y <= 1:
    print(Brown)
    exit()
if X-Y == 2:
    print(Alice)
    exit()

if Y == 0:
    print(Alice)
    exit()

if ((X-Y) % 3 == 0) or ((X-Y) % 3 == 1):
    print(Alice)
    exit()

print(Brown)
