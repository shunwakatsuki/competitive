#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)
from random import randrange
from datetime import datetime


start = datetime.now()
N = 100
A = []                          # type: List[List[int]]
for n in range(N):
    row = list(map(int, input().split()))
    A.append(row)


def yama(B, x, y, h):
    for i in range(N):
        for j in range(N):
            B[i][j] += max(0, h - abs(i-x) - abs(j-y))


def height(B, x, y, d):
    h = N
    for i in range(x-d, x+d):
        for j in range(y-d, y+d):
            if (0 <= i < N) and (0 <= j < N):
                if A[i][j] < B[i][j]:
                    return 0
                dist = abs(i-x) + abs(j-y)
                h = min(h, dist+A[i][j]-B[i][j])
    return h

B = [[0]*N for _ in range(N)]
Q = 0
params = []
d = 10
while (Q < 1000) and ((datetime.now() - start).seconds < 5):
    x = randrange(N)
    y = randrange(N)
    # h_max = min(N, max(0, A[x][y] - B[x][y]))
    h_max = height(B, x, y, d)
    if h_max >= 1:
        h = randrange(min(50, h_max), h_max+1)
        # h = h_max
        params.append((x, y, h))
        Q += 1
        yama(B, x, y, h)
print(Q)
for p in params:
    x, y, h = p
    print(y, x, h)
