#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = 100
A = []                          # type: List[List[int]]
for n in range(N):
    row = list(map(int, input().split()))
    A.append(row)


print(0)
