#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)
from random import randrange
from datetime import datetime


start = datetime.now()
N = 100
A = []                          # type: List[List[int]]
for n in range(N):
    row = list(map(int, input().split()))
    A.append(row)


def yama(B, x, y, h):
    for i in range(N):
        for j in range(N):
            B[i][j] += max(0, h - abs(i-x) - abs(j-y))


B = [[0]*N for _ in range(N)]
Q = 0
params = []
while (Q < 1000) and ((datetime.now() - start).seconds < 6):
    x = randrange(N)
    y = randrange(N)
    h_max = min(N, max(0, A[x][y] - B[x][y]))
    if h_max >= 1:
        h = randrange(1, h_max+1)
        params.append((x, y, h))
        Q += 1
        yama(B, x, y, h)
print(Q)
for p in params:
    x, y, h = p
    print(x, y, h)
