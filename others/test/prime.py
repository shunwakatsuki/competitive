#!/usr/bin/env python3
import math


def is_prime(p):
    # type: (int) -> bool
    "正の整数 p が素数かどうか判定"
    if p <= 0:
        raise Exception("素数判定は正の整数に対してのみ")
    for i in range(2, int(math.sqrt(p)+1)):
        if p % i == 0:
            return False
    return True


class IntMod:
    def __init__(self, n, p):
        # type: (int, int) -> None
        "整数 n の mod p での値を計算する(p は素数)"
        self.n = n % p
        self.p = p

    def __add__(self, other):
        return IntMod(self.n + other.n, self.p)

    def __neg__(self):
        return IntMod(-self.n, self.p)

    def __sub__(self, other):
        return IntMod(self.n - other.n, self.p)

    def __mul__(self, other):
        return IntMod(self.n * other.n, self.p)

    def __pow__(self, exp):
        if self.n == 0:
            if exp < 0:
                raise Exception("0の負べきは無理")
            return 0
        n = self.n
        p = self.p
        exp = exp % (p-1)  # >= 0
        res = 1
        temp = n
        while exp > 0:
            if exp % 2 == 1:
                res = (res * temp) % p
            exp = exp // 2
            temp = temp ** 2
        return IntMod(res, p)

    def __truediv__(self, other):
        return self * (other ** (-1))

    def __str__(self):
        return "%d (mod %d)" % (self.n, self.p)


if __name__ == '__main__':
    # for p in range(2, 100):
    #     print(p, is_prime(p))
    n = IntMod(5, 7)
    print(n ** (-1))
