#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


H, W, A, B = map(int, input().split())


p = 10**9 + 7
def power(n, e):
    e = e % (p-1)  # >= 0
    res = 1
    temp = n
    while e > 0:
        if e % 2 == 1:
            res = (res * temp) % p
        e = e // 2
        temp = temp ** 2
    return res


def comb(n, m):
    "choose n from (n+m)"
    if n > m:
        n, m = m, n
    # assert n <= m
    # res = IntMod(1, p)
    bunshi = 1
    bunbo = 1
    for i in range(1, n+1):
        bunshi = bunshi * (m+i) % p
        bunbo = bunbo * i % p
    res = bunshi * power(bunbo, p-2) % p
    return res


ans = 0
for i in range(1, H-A+1):
    ans += comb(i-1, B-1) * comb(H-i, W-B-1)

print(ans)
