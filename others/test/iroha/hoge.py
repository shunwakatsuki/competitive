#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


class IntMod:
    def __init__(self, n, p):
        # type: (int, int) -> None
        "整数 n の mod p での値を計算する(p は素数)"
        self.n = n % p
        self.p = p

    def __add__(self, other):
        return IntMod(self.n + other.n, self.p)

    def __neg__(self):
        return IntMod(-self.n, self.p)

    def __sub__(self, other):
        return IntMod(self.n - other.n, self.p)

    def __mul__(self, other):
        return IntMod(self.n * other.n, self.p)

    def __pow__(self, exp):
        debug(self.n, exp)
        if self.n == 0:
            if exp < 0:
                raise Exception("0の負べきは無理")
            return 0
        n = self.n
        p = self.p
        exp = exp % (p-1)  # >= 0
        res = 1
        temp = n
        while exp > 0:
            if exp % 2 == 1:
                res = (res * temp) % p
            exp = exp // 2
            temp = temp ** 2
        return IntMod(res, p)

    def __truediv__(self, other):
        return self * (other ** (-1))

    def __str__(self):
        return str(self.n) # "%d (mod %d)" % (self.n, self.p)


p = 10**9 + 7
def comb(n, m):
    "choose n from (n+m)"
    debug(n, m)
    if n > m:
        n, m = m, n
    # assert n <= m
    # res = IntMod(1, p)
    bunshi = IntMod(1, p)
    bunbo = IntMod(1, p)
    for i in range(1, n+1):
        bunshi = bunshi * IntMod(m+i, p)
        bunbo = bunbo * IntMod(i, p)
    res = bunshi / bunbo
    return res


H, W, A, B = map(int, input().split())

ans = IntMod(0, p)
for i in range(1, H-A+1):
    ans += comb(i-1, B-1) * comb(H-i, W-B-1)

print(ans)
