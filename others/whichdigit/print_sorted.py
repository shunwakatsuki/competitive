#!/usr/bin/env python3

N, D = map(int, input().split())
S = str(input())

seq_list = [S[i:N] for i in range(N)]
seq_list.sort()

for i in range(len(seq_list)):
    print(str(i).zfill(2), seq_list[i])
