#!/usr/bin/env python3
# import datetime

# print(datetime.datetime.now().strftime("%H:%M:%S"), "loading")

N, D = map(int, input().split())
S = str(input())

def count_with_overlap(s, sub):
    start = 0
    count = 0
    while True:
        start = s.find(sub, start) + 1
        if start > 0:
            count += 1
        else:
            return count

def find(ith, prefix=""):
    # print("find(ith=%s, prefix=%s)"%(ith, prefix))
    total = 0
    if (len(prefix) > 0) and (S.endswith(prefix)):
        if ith == 0:
            return N - len(prefix)
        total += 1
    for p in "0123456789":
        c = count_with_overlap(S, prefix+p) # S.count(prefix+p)
        # print('p="%s", total=%s, c=%s, total+c=%s, ith-total-c=%s'%(p, total, c, total+c, ith-total-c))
        if total + c - 1 >= ith:
            if c == 1:
                return S.find(prefix+p)
            return find(ith-total, prefix+p)
        total += c

# print(datetime.datetime.now().strftime("%H:%M:%S"), "start")
for d in range(1,D):
    i = (N//D) * d - 1
    print(find(i)+1)
# print(datetime.datetime.now().strftime("%H:%M:%S"), "end")
