#!/usr/bin/env python3

N, D = map(int, input().split())
S = str(input())

seq_list = [S[i:N] for i in range(N)]

# def sort_by_prefix(sl, pre):
#     return sorted(sl, key=lambda s: s[:pre])
    # slwp = list(map(lambda s: (s, s[:pre])), sl)
    # return sorted(slwp, key=lambda s: s[1])

def simple_find(sl, ith, removed_len):
    return len(sorted(sl)[ith])

def get_head(s):
    if s == "":
        return "."
    return s[0]

def find(sl, ith, removed_len):
    # print(sl, ith, removed_len)
    sl_sorted = sorted(sl, key=get_head)
    head = sl_sorted[ith][0]
    for i in range(ith+1):
        if sl_sorted[i][0] == head:
            start = i
            break
    for i in range(ith+1, len(sl_sorted)):
        if sl_sorted[i][0] != head:
            end = i
            break
    else:
        end = len(sl_sorted)
    if start+1 == end:
        return removed_len + len(sl_sorted[ith])
    sl_new = list(map(lambda s: s[1:],
                      sl_sorted[start:end]))
    return find(sl_new, ith-start, removed_len+1)

for d in range(1, D):
    i = (N//D) * d - 1
    print(N+1 - simple_find(seq_list, i, 0))
