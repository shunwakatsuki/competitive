#include <iostream>
using namespace std;

int main()
{
  int N;
  cin >> N;
  int A[300];
  for (int i=0; i<N; i++){
    cin >> A[i];
  }
  int count = 0;
  bool finished = false;
  while (true){
    for (int i=0; i<N; i++){
      if (A[i]%2 == 1){
        finished = true;
      }
      A[i] = A[i]/2;
    }
    if (finished){
      break;
    }
    count++;
  }
  cout << count << endl;
  return 0;
}
