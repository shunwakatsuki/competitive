#!/usr/bin/env python3
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass


# 最大公約数
def gcd(a, b):
    # type: (int, int) -> int
    "最大公約数を計算する．0とか負の数が入るとヤバい"
    if b%a == 0:
        return a
    return gcd(b%a, a)
def gcd_neg(a, b):
    # type: (int, int) -> int
    "0や負の数にも対応したversion"
    if a < 0:
        a = -a
    if b < 0:
        b = -b
    if b < a:
        b, a = a, b
    if a == 0:
        return b
    return gcd(a, b)


# 素因数分解
def prime_factorization(n):
    factors = {}
    q = 2
    while n != 1:
        if q**2 > n:
            factors[n] = 1
            break
        while n % q == 0:
            n = n // q
            if q in factors:
                factors[q] += 1
            else:
                factors[q] = 1
        q += 1
    return factors


# 回文判定
def is_palind(a):
    # type: (str) -> bool
    for i in range(len(a) // 2):
        if a[i] != a[-i-1]:
            return False
    return True


# 色々と細かいやつ
def kiriage(a, b):
    # type: (int, int) -> int
    "a/b を切り上げ"
    return (a + b - 1) // b


def sum_of_digits(n):
    # type: (int) -> int
    "各桁の和"
    return sum(map(int, str(n)))


# 木
class UnionFind:
    def __init__(self, N):
        # type: (int) -> None
        self.N = N
        self.parent = list(range(N))  # type: List[int]

    def root(self, x):
        # type: (int) -> int
        path_to_root = []
        while self.parent[x] != x:
            path_to_root.append(x)
            x = self.parent[x]
        for node in path_to_root:
            self.parent[node] = x  # パス圧縮
        return x

    def same(self, x, y):
        # type: (int, int) -> bool
        return self.root(x) == self.root(y)

    def unite(self, x, y):
        # type: (int, int) -> None
        self.parent[self.root(x)] = self.root(y)

    def __str__(self):
        # type: () -> str
        groups = {}  # type: Dict[int, List[int]]
        for x in range(self.N):
            root = self.root(x)
            if root in groups.keys():
                groups[root].append(x)
            else:
                groups[root] = [x]
        result = ""
        for root in groups.keys():
            result += str(groups[root]) + "\n"
        return result

    def __len__(self):
        # type: () -> int
        roots = list(map(lambda x: self.root(x),
                         range(self.N)))
        return len(set(roots))


def kruskal(num_vertices, edges):
    # type: (int, List[Tuple[int, int, int]]) -> int
    """Kruscal法: 最小全域木を求める
    edgesについて破壊的なことに注意
    """
    total_weight = 0
    edges.sort(key=lambda e: e[2])
    u = UnionFind(num_vertices)
    for e in edges:
        x = e[0]
        y = e[1]
        weight = e[2]
        if u.same(x, y):
            continue
        u.unite(x, y)
        total_weight += weight
    return total_weight
