#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)
import math


n, p = map(int, input().split())


primes = {}
q = 2
while p != 1:
    if q**2 > p:
        primes[p] = 1
        break
    while p % q == 0:
        p = p // q
        if q in primes:
            primes[q] += 1
        else:
            primes[q] = 1
    q += 1


ans = 1
for q in primes:
    ans *= q**(primes[q] // n)
print(ans)
