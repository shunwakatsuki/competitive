#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


n = int(input())
a = list(map(int, input().split()))


k = []
for x in range(n-1):
    k_ = 0
    while True:
        if (4**k_) * a[x+1] >= a[x]:
            break
        k_ += 1
    k.append(k_)
l = [None]                      # type: List[Union[int, None]]
for x in range(1, n):
    l_ = 0
    while True:
        if (4**l_) * a[x-1] >= a[x]:
            break
        l_ += 1
    l.append(l_)

M = [0]
for x in range(1, n):
    s = a[x-1] * (2**M[x-1])
    t = a[x]
    v = 0
    while True:
        if (4**v) * t >= s:
            break
        v += 1
    M.append(2*v)
    # M.append(M[-1] + 2*k[x])


temp = sum(M)
cand = [temp]

for i in range(n-1):
    if i == 0:
        u = 1
    else:
        u = 1 + 2*l[i]
    if i == n-1:
        v = 0
    else:
        v = 2*k[i]
    temp += i*u - (n-i-2)*v + 1 - i
    cand.append(temp)

print(min(cand))

debug(cand)
for i in range(n-1):
    debug(a[i+1]/a[i])
