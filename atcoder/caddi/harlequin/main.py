#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


n = int(input())
a = []
for _ in range(n):
    a.append(int(input()))


for i in range(n):
    if a[i] % 2 == 1:
        print("first")
        exit()
print("second")
