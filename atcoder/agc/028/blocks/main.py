#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
A = list(map(int, input().split()))
p = 10**9 + 7


f = 1
for i in range(2, N):
    f = (f * i) % p


def power(x, n):
    if n == 0:
        return 1
    if n % 2 == 0:
        return power(x*x, n//2) % p
    else:
        return x * power(x, n-1) % p

def inv(a):
    return power(a, p-2)

s = 0
for k in range(1, N+1):
    s += ((N-k) * inv(k)) % p


small = (f * (N + s)) % p
big = (f * (N + 2*s)) % p


ans = 0
if N == 1:
    pass
ans += ((A[0]+A[N-1]) * small) % p
for i in range(1, N-1):
    ans += (A[i] * big) % p

print(ans)
