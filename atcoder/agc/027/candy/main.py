#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N, x = map(int, input().split())
a = list(map(int, input().split()))


a.sort()


i = 0
s = 0
while (i<N) and (s <= x):
    s += a[i]
    i += 1


if i == N:
    if sum(a) == x:
        print(N)
    else:
        print(N-1)
else:
    print(i-1)
