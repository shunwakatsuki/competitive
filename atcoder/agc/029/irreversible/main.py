#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


S = input()


count = 0
ans = 0
for s in S:
    if s == "B":
        count += 1
    else:
        ans += count
print(ans)
