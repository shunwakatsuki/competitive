#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N, K = map(int, input().split())
A = list(map(int, input().split()))

p = 10**9 + 7


def log(n):
    if n == 0:
        return 0
    k = 0
    while 2**(k) <= n:
        k += 1
    return k
def prod(_list):
    # return reduce(operator.mul, _list, 1)
    res = 1
    for l in _list:
        res *= l % p
    return res


a = list(map(log, A))


if K >= sum(a):
    ans = prod(map(lambda x:x+1, a)) - prod(a)
    print(ans % p)
    debug("large K")
    exit()

#
count = []
count.append([1]*(a[0]+1) + [0]*(K-a[0]))
for n in range(1, N):
    count.append([])
    for k in range(K+1):
        s = 0
        m = max(0, k-a[n])
        for j in range(m, k+1):
            s += count[n-1][j] % p
        count[n].append(s)


if min(a) == 0:
    count_ = [[0]*(K+1)]*N
else:
    count_ = []
    a_ = list(map(lambda x: x-1, a))
    count_.append([1]*(a_[0]+1) + [0]*(K-a_[0]))
    for n in range(1, N):
        count_.append([])
        for k in range(K+1):
            s = 0
            m = max(0, k-a_[n])
            for j in range(m, k+1):
                s += count_[n-1][j] % p
            count_[n].append(s)


ans = count[N-1][K]
for k in range(K):
    ans += count[N-1][k] - count_[N-1][k]
debug("hoge")
print(ans%(10**9+7))
