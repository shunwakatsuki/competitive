#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N, M, A, B = map(int, input().split())
L = []
R = []
for j in range(M):
    l, r = map(int, input().split())
    L.append(l-1)
    R.append(r-1)


flags = [False]*N
for j in range(M):
    for i in range(L[j], R[j]+1):
        flags[i] = True

ans = 0
for i in range(N):
    if flags[i]:
        ans += A
    else:
        ans += B


print(ans)
