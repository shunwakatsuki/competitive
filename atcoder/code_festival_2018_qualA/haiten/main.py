#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


a = int(input())
b = int(input())
c = int(input())
s = int(input())


if a + b + c <= s <= a + b + c + 3:
    print("Yes")
else:
    print("No")
