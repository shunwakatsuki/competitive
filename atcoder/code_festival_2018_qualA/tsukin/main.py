#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


D, F, T, N = map(int, input().split())
X = [0] + list(map(int, input().split()))

p = 10**9 + 7


f = [1] + [0]*N
# i = 0
# k = 0
# j = 1
# while True:
#     # debug(i, k, j)
#     if j > N:
#         break
#     if X[j]-X[i] > F:
#         i += 1
#         continue
#     if X[j]-X[i] <= F-T:
#         j += 1
#         continue
#     if k < i:
#         k = i
#     while X[k+1] <= X[i] + F-T:
#         k += 1
#         if k+1 > N:
#             break
#     debug(i,k,j)
#     f[j] = (f[j] + (2**(k-i)) * f[i]) % p
#     i += 1
# debug(f)
for j in range(N+1):
    for i in range(j):
        if F-T < X[j]-X[i] <= F:
            k = i
            while X[k+1] <= X[i] + F-T:
                k += 1
            f[j] += 2**(k-i) * f[i]

ans = 0
for i in range(N+1):
    if D - X[i] <= F:
        k = i
        while (k+1 <= N) and X[k+1] - X[i] <= F-T:
            k += 1
        ans = (ans + (2**(k-i)) * f[i]) % p
print(ans)
