#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


# def kiriage(a, b):
#     return (a + b - 1) // b


if __name__ == '__main__':
    N, K = map(int, input().split())
    A = list(map(int, input().split()))

    i = A.index(1)

    # print(kiriage(i, K-1) + kiriage(N-i-1, K-1))
    l = i // (K-1)
    r = (N-i-1) // (K-1)

    rest_left = i - l*(K-1)
    rest_right = (N-i-1) - r*(K-1)
    rest = N - (l+r) * (K-1)


    if rest_left == 0 and rest_right == 0:
        print(l+r)
    elif rest_left == 0 or rest_right == 0:
        print(l+r+1)
    elif rest <= K:
        print(l + r + 1)
    else:
        print(l+r+2)
