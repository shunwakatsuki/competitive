#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


K = int(input())

# if K <= 9:
#     for i in range(1, K+1):
#         print(i)
#     exit()

# for i in range(1, 10):
#     print(i)


def S(n):
    res = 0
    while n > 0:
        res += n % 10
        n = n // 10
    return res

def is_snuke(n):
    a = n / S(n)
    for i in range(1, 100000):
        m = n + 1 * i
        if a > m / S(m):
            return False
    return True

for n in range(1, 1000000):
    # n = 1009 + 10*i
    if is_snuke(n):
        debug(n)

# current = 19
# K = K-9
# while K > 0:
#     if is_snuke(current):
#         print(current)
#         current += 10
#         K -= 1

snukes = [None, 1, 2, 3, 4, 5, 6, 7, 8, 9, 19, 29, 39, 49, 59, 69, 79, 89, 99, 199, 299, 399, 499, 599, 699, 799, 899, 999, 1099, 1199, 1299, 1399, 1499, 1599, 1699, 1799, 1899, 1999, 2999, 3999, 4999, 5999, 6999, 7999, 8999, 9999]

l = len(snukes)

if K <= l:
    for i in range(1, K+1):
        print(snukes[i])
    exit()

for i in range(1, l+1):
    print(snukes[i])
