#!/usr/bin/env python3
import sys
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


K = int(input())

def S(n):
    res = 0
    while n > 0:
        res += n % 10
        n = n // 10
    return res


def is_snuke(e, a):
    n = a*(10**e) + (10**e-1)
    nS = n / S(n)
    for i in range(1, 200):
        m = n + i * (10**e)
        if nS > m / S(m):
            return False
    return True

def is_snuke_small(n):
    nS = n / S(n)
    for i in range(1000):
        m = n + i
        if nS > m / S(m):
            return False
    return True

snukes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 19, 29, 39, 49, 59, 69, 79, 89, 99, 199, 299, 399, 499, 599, 699, 799, 899, 999, 1099, 1199, 1299, 1399, 1499, 1599, 1699, 1799, 1899, 1999, 2999, 3999, 4999, 5999, 6999, 7999, 8999, 9999, 10999, 11999, 12999, 13999, 14999, 15999, 16999, 17999, 18999, 19999, 20999, 21999, 22999, 23999, 24999, 25999, 26999, 27999, 28999, 29999, 39999, 49999, 59999, 69999, 79999, 89999, 99999, 109999, 119999, 129999, 139999, 149999, 159999, 169999, 179999, 189999, 199999, 209999, 219999, 229999, 239999, 249999, 259999, 269999, 279999, 289999, 299999, 309999, 319999, 329999, 339999, 349999, 359999, 369999, 379999, 389999, 399999, 499999, 599999, 699999, 799999, 899999, 999999]

def keta(n):
    keta = 0
    while n > 0:
        keta += 1
        n = n // 10
    return keta


def next(n):
    e = keta(n)
    if e <= 2:
        return n+1
    return n + 10**(e-2)

n = 1
while n < 100000:
    debug(n)
    n = next(n)
exit()
while K > 0:
    if n < 1000:
        if is_snuke_small(n):
            print(n)
            K -= 1
    e = keta(n) - 2
    n = next(n)
    # n = a*(10**e) + (10**e-1)
    # if n >= 10**(e+1):
    #     e = e+1
    #     a = 1
    #     continue
    # if is_snuke(e, a):
    #     print(n)
    #     K -= 1
    #     a += 1
    #     if snukes:
    #         debug(n, snukes.pop(0))
