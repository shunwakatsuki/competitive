K = int(input())

def S(n):
    return n / sum(map(int, str(n)))

num = min(K, 8)
for n in range(1, num + 1):
    print(n)

d = 10
n = 10
Sn = S(10 - 1)
while num < K:
    for j in range(100):
        Snext = S(n + d - 1)
        if Snext < Sn: break

        print(n - 1)
        num += 1
        if num == K: break

        n += d
        Sn = Snext

    d *= 10
    n = (n // d + 1) * d
