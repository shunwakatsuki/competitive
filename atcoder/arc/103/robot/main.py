#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
P = []
last = []
parity = -1
for i in range(N):
    X, Y = map(int, input().split())
    if parity == -1:
        parity = (X+Y) % 2
    else:
        if parity != (X+Y) % 2:
            print(-1)
            exit()
    if (X%2==0 and Y%2==0) or (X%2==1 and Y%2==0):
        X = X-1
        last.append("R")
    else:
        Y = Y-1
        last.append("U")
    P.append((X, Y))

num = 22-parity
print(num)
for i in range(num):
    print(1, end=" ")
print()
for i in range(N):
    X = P[i][0]
    Y = P[i][1]
    res = []
    if X > 0:
        for j in range(X):
            res.append("R")
        for j in range((11-X)//2):
            res.append("RL")
    if X < 0:
        for j in range(-X):
            res.append("R")
        for j in range((11+X)//2):
            res.append("RL")
    if Y > 0:
        for j in range(Y):
            res.append("U")
        for j in range((10-Y)//2):
            res.append("UD")
    if Y < 0:
        for j in range(-Y):
            res.append("U")
        for j in range((10+Y)//2):
            res.append("UD")
    res.append(last[i])
    print("".join(res))



# dx = []
# for i in range(N-1):
#     Xi = P[i][0]
#     Xi1 = P[i+1][0]
#     if (Xi-Xi1)%2 != 0:
#         raise Exception("parity: x")
#     dx.append((Xi-Xi1)//2)
# dx.append(P[N-1][0]+sum(dx))

# dy = []                         # type: List[int]
# for i in range(N-1):
#     Yi = P[i][1]
#     Yi1 = P[i+1][1]
#     if (Yi-Yi1)%2 != 0:
#         raise Exception("parity: y")
#     dy.append((Yi-Yi1)//2)
# dy.append(P[N-1][1]+sum(dy))


# def abs(q):
#     if q < 0:
#         return -q
#     else:
#         return q

# nz = dx.count(0) + dy.count(0)
# print(2*N+1-nz)
# for i in range(N):
#     if dx[i] == 0:
#         continue
#     print(abs(dx[i]), end=" ")
# for i in range(N):
#     if dy[i] == 0:
#         continue
#     print(abs(dy[i]), end=" ")
# print(1)

# for n in range(N):
#     ans = []                    # type: List[str]
#     for i in range(N):
#         if dx[i] == 0:
#             continue
#         if i >= n:
#             sign = 1
#         else:
#             sign = -1
#         if dx[i]*sign > 0:
#             ans.append("R")
#         else:
#             ans.append("L")
#     for i in range(N):
#         if dy[i] == 0:
#             continue
#         if i >= n:
#             sign = 1
#         else:
#             sign = -1
#         if dy[i]*sign > 0:
#             ans.append("U")
#         else:
#             ans.append("D")
#     ans.append(last[n])
#     print("".join(ans))
