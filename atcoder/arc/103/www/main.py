#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

from collections import Counter

n = int(input())
v = list(map(int, input().split()))

c = Counter(v)
if len(c) == 1:
    print(n//2)
    exit()
# fc = c.most_common(2)
# count1 = fc[0][1]
# count2 = fc[1][1]
# ok = 0
# debug(fc)

# if count1 <= n//2:
#     ok += count1
# else:
#     ok += n//2
# ok += count2


# print(n-ok)

odd = [v[2*i+1] for i in range(n//2)]
even = [v[2*i] for i in range(n//2)]
# odd_count = Counter(odd).most_common(1)[0][1]
# even_count = Counter(even).most_common(1)[0][1]

oc = Counter(odd).most_common()
ec = Counter(even).most_common()

if oc[0][0] != ec[0][0]:
    odd_count = oc[0][1]
    even_count = ec[0][1]
    both_count = odd_count + even_count
else:
    # if oc[0][1] > ec[0][1]:
    #     odd_count = oc[0][1]
    #     even_count = ec[1][1]
    # else:
    #     odd_count = oc[1][1]
    #     even_count = ec[0][1]
    both1 = oc[0][1] + ec[1][1]
    both2 = oc[1][1] + ec[0][1]
    both_count = max(both1, both2)

print(n-both_count)
