#!/usr/bin/env python3

N = int(input())
S = str(input())

west_w = 0
west_e = 0
east_w = S[1:].count("W")
east_e = S[1:].count("E")

min_change = west_w + east_e
for i in range(1, N):
    if S[i-1] == "W":
        west_w += 1
    else:
        west_e += 1
    if S[i] == "W":
        east_w -= 1
    else:
        east_e -= 1
    change = west_w + east_e
    if change < min_change:
        min_change = change

print(min_change)
