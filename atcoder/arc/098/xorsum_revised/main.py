#!/usr/bin/env python3
# 尺取り法を意識して実装しなおしてみた

N = int(input())
A = list(map(int, input().split()))

r = 0
ans = 0

current_sum = A[0]

for l in range(N):
    ans += r - l + 1
    while r < N-1:
        if current_sum + A[r+1] == current_sum ^ A[r+1]:
            current_sum += A[r+1]
            ans += 1
            r += 1
        else:
            current_sum -= A[l]
            break

print(ans)
