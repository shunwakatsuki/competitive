#!/usr/bin/env python3

N = int(input())
A = list(map(int, input().split()))

A = list(map(lambda x: bin(x)[2:].zfill(20),
             A))

admissible = [N-1] * N

def update(i, adm):
    if adm < admissible[i]:
        admissible[i] = adm

for k in range(20):
    # A[*][k]
    prev = []
    curr = []
    for i in range(N):
        # if A[i][k] == "0":
        #     curr.append(i)
        # else:
        curr.append(i)
        if A[i][k] == "1":
            for j in prev:
                update(j, i-1)
            prev = curr
            curr = []


# print(admissible)
ans = 0
for i in range(N):
    ans += admissible[i] - i + 1
print(ans)
