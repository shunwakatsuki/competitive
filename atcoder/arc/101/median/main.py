#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
a = [-1] + list(map(int, input().split()))


def count(ind, width):
    if ind <= width:
        return ind
    elif width <= ind <= N - width + 1:
        return width
    else:
        return N - ind + 1


harm = [0.0]
sum_ = 0.0
for i in range(1,N+1):
    sum_ += 1/i
    harm.append(sum_)
hoge = [0.0]
sum_ = 0.0
for i in range(1,N+1):
    sum_ += i / (N-i+1)
    hoge.append(sum_)


b = [(0,0.0)]
for i in range(1,N+1):
    j = min(i, N-i+1)
    l = max(i, N-i+1)
    x = j + j*(harm[l]-harm[j]) + (hoge[j])
    b.append((a[i], x))


b.sort(key=lambda z: z[1])
s = 0.0
i = 1
while s < (N*(N+1)//2)//2+1:
    s += b[i][1]
    i += 1
print(b[i][0])
