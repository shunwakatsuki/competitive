#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N, K = map(int, input().split())
x = list(map(int, input().split()))


# s = [0]  # type: List[int]
# sum_ = 0
# for i in range(N):
#     sum_ += x[i]
#     s.append(sum_)
def abs(n):
    if n >= 0:
        return n
    else:
        return -n
times = []                      # type: List[int]
for i in range(N-K+1):
    t = x[i+K-1] - x[i]
    t += min(abs(x[i+K-1]), abs(x[i]))
    times.append(t)

debug(times)
print(min(times))
