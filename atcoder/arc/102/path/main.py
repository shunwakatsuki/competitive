#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


L = int(input())


def sanshin(x):
    res = []
    while x > 0:
        res.append(x % 3)
        x = x // 3
    return res


a = sanshin(L)
b = []
L_ = L
for i in range(len(a)):
    L_ -= a[i] * 3**i
    b.append(L_)

N = 14
result = []

for i in range(N-1):
    result.append((i, i+1, 0))
    result.append((i, i+1, 3**i))
    if 2*(3**i) < 10**6:
        result.append((i, i+1, 2*(3**i)))


for i in range(N):
    if i >= len(a):
        break
    if a[i] == 1:
        result.append((i, N, b[i]))
    elif a[i] == 2:
        result.append((i, N, b[i]))
        result.append((i, N, b[i] + 3**i))


M = len(result)
print(N+1, M)
for res in result:
    print(res[0]+1, res[1]+1, res[2])
