#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N, K = map(int, input().split())

if K % 2 == 1:
    NK = N // K
    NK2 = 0
else:
    NK = N // K
    NK2 = N // (K//2) - NK
debug(NK, NK2)
print(NK**3 + NK2**3)
