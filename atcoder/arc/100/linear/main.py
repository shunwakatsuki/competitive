#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)


N = int(input())
A = list(map(int, input().split()))

min_ = 10**20

B = []
for i in range(N):
    B.append(A[i]-i-1)

B.sort()

Bplus = sum(B)
Bminus = 0
for n in range(N//2):
    sad = Bplus - Bminus
    sad += (2*n-N)*B[n]
    min_ = min(sad, min_)

    Bplus -= B[n]
    Bminus += B[n]

for n in range(N//2, N):
    sad = Bplus - Bminus
    sad += (2*n-N)*B[n-1]
    min_ = min(sad, min_)

    Bplus -= B[n]
    Bminus += B[n]

print(min_)
