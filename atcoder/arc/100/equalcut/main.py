#!/usr/bin/env python3
import sys
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
def debug(*args): print(*args, file=sys.stderr)
def exit(): sys.exit(0)

N = int(input())
A = list(map(int, input().split()))

S = []
sum_ = 0
for i in range(N):
    sum_ += A[i]
    S.append(sum_)

def search(s, prev_S, l=0, r=N):
    # return maximum i with (S[i]-prev_S <= s) and (l <= i < r)
    if r-l <= 1:
        return l
    m = l + (r-l)//2
    if S[m] - prev_S > s:
        return search(s, prev_S, l, m)
    else:
        return search(s, prev_S, m, r)


min_ = 10**20
for i in range(1, N-2):
    if i == 3:
        flag = True
    else:
        flag = False
    li = search(S[i]/2, 0, 0, i+1)
    ri = search((S[N-1]-S[i])/2, S[i], i+1, N)
    candidates = [(li, i, ri),   (li+1, i, ri),
                  (li, i, ri+1), (li+1, i, ri+1)]
    for p, q, r in candidates:
        # debug(p, q, r)
        if r >= N:
            continue
        P = S[p]
        Q = S[q] - S[p]
        R = S[r] - S[q]
        S_ = S[N-1] - S[r]
        cur_min = min(P, Q, R, S_)
        cur_max = max(P, Q, R, S_)
        min_ = min(min_, cur_max-cur_min)
    # debug(li, i, ri, min_)

print(min_)
