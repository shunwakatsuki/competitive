#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


a, b = map(int, input().split())
c, d = map(int, input().split())
e, f = map(int, input().split())


count = {1: 0, 2: 0, 3: 0, 4: 0}


for i in [a, b, c, d, e, f]:
    count[i] += 1

count_list = []
for key in count.keys():
    count_list.append(count[key])

m = max(count_list)
if m >= 3:
    print("NO")
else:
    print("YES")
