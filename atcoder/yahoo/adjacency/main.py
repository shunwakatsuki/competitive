#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


n, k = map(int, input().split())


if n % 2 == 0:
    m = n // 2
else:
    m = n // 2 + 1

if k <= m:
    print("YES")
else:
    print("NO")
