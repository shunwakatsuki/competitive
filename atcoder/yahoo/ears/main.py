#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


l = int(input())
a = []
for _ in range(l):
    a.append(int(input()))

left = [0]
i = 0
j = 1
score = 0
score_reduce = 0
while j <= l:
    if a[j-1] == 0:
        score_reduce += 2
    elif a[j-1] % 2 == 0:
        score_reduce -= a[j-1]
    else:
        score_reduce -= a[j-1] - 1
    if a[i] <= score_reduce:
        i = j
        score += a[i] - score_reduce
        score_reduce -= a[i]
    else:
        if a[j-1] == 0:
            score += 2
        elif a[j-1] % 2 == 0:
            pass
        else:
            score += 1
    left.append(score)
    j += 1
debug(left)

right = [0] * (l+1)
i = l
j = l-1
score = 0
score_reduce = 0
while j >= 0:
    if a[j] == 0:
        score_reduce += 2
    elif a[j] % 2 == 0:
        score_reduce -= a[j]
    else:
        score_reduce -= a[j] - 1
    if a[i-1] <= score_reduce:
        i = j
        score += a[i-1] - score_reduce
        score_reduce -= a[i-1]
    else:
        if a[j] == 0:
            score += 2
        elif a[j] % 2 == 0:
            pass
        else:
            score += 1
    right[j] = score
    j -= 1
debug(right)


i = 0
j = 0
total_min = right[0]
score_reduce = 0
sub = 0
while j < l:
    if a[j] == 0:
        pass
    elif a[j] % 2 == 0:
        sub += 1
    else:
        pass
    total = left[i] + right[j] + sub
    another = left[j] + right[j]
    if another <= total:
        i = j
    total_min = min(total, another)
    j += 1
print(total_min)
