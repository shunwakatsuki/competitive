#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


l = int(input())
a = []
for _ in range(l):
    a.append(int(input()))

init_score = 0
for i in range(l):
    if a[i] == 0:
        init_score += 2
    elif a[i] % 2 == 0:
        pass
    else:
        init_score += 1

left = [0]
for i in range(l):
    left.append(left[i])
    if a[i] == 0:
        left[i+1] -= 2
    elif a[i] % 2 == 0:
        left[i+1] += a[i]
    else:
        left[i+1] += a[i] - 1
left_i = 0
left_min = left[0]
for i in range(l+1):
    if left[i] < left_min:
        left_i = i
        left_min = left[i]
debug(left_i, left)

right = [0] * (l+1)
for i in range(l, 0, -1):
    right[i-1] = right[i]
    if a[i-1] == 0:
        right[i-1] -= 2
    elif a[i-1] % 2 == 0:
        right[i-1] += a[i-1]
    else:
        right[i-1] += a[i-1] - 1
right_i = l
right_min = right[l]
for i in range(l, -1, -1):
    if right[i] < right_min:
        right_i = i
        right_min = right[i]
debug(right_i, right)

assert(left_i <= right_i)

# odd
temp_score = init_score + left_min + right_min
b = a[left_i:right_i]
k = len(b)

for i in range(k):
    if b[i] == 0:
        temp_score -= 1
    elif b[i] % 2 == 0:
        temp_score += 1
    else:
        temp_score -= 1


left = [0]
for i in range(k):
    left.append(left[i])
    if b[i] == 0:
        left[i+1] += 1
    elif b[i] % 2 == 0:
        left[i+1] -= 1
    else:
        left[i+1] += 1
left_i = 0
left_min = left[0]
for i in range(k+1):
    if left[i] < left_min:
        left_i = i
        left_min = left[i]
debug(left_i, left)

right = [0] * (k+1)
for i in range(k, 0, -1):
    right[i-1] = right[i]
    if b[i-1] == 0:
        right[i-1] += 1
    elif b[i-1] % 2 == 0:
        right[i-1] -= 1
    else:
        right[i-1] += 1
right_i = k
right_min = right[k]
for i in range(k, -1, -1):
    if right[i] < right_min:
        right_i = i
        right_min = right[i]
debug(right_i, right)

assert(left_i <= right_i)

score = temp_score + left_min + right_min
print(score)
