#!/usr/bin/env python3
import sys, os
try: from typing import Any, Union, List, Tuple, Dict
except ImportError: pass
sys.setrecursionlimit(10**6)
if os.environ.get("USER") == "shun":
    def debug(*args): print(*args, file=sys.stderr)
else:
    print("NO DEBUG", file=sys.stderr)
    def debug(*args): pass
def exit(): sys.exit(0)


k, a, b = map(int, input().split())


if b - a <= 1:
    print(k + 1)
else:
    if k < a:
        print(k+1)
        exit()
    remain = k - (a-1)
    x = remain // 2
    y = remain - 2*x
    print(a + x*(b-a) + y)
